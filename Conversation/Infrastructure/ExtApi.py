from pyfacebook import Api, BaseModel

class ExtApi(Api):
#     DEFAULT_CONVERSATION_FIELDS = [
#         "id", "link", "snippet", "updated_time", "message_count",
#         "unread_count", "participants", "senders", "can_reply",
#         "is_subscribed",
#     ]
    DEFAULT_CONVERSATION_FIELDS = ["id"]
    def page_by_next(self,
                     target,  # type: str
                     resource,  # type: str
                     args,  # type: Dict
                     next_page,  # type: str
                     ):
        if next_page is not None:
            resp = self._request(
                path=next_page
            )
        else:
            resp = self._request(
                path="{version}/{target}/{resource}".format(
                    version=self.version, target=target, resource=resource
                ),
                args=args
            )
        next_page = None
        data = self._parse_response(resp)
        if "paging" in data:
            next_page = data["paging"].get("next")
        return next_page, data
    def get_page_conversations(self,
                               page_id,  # type: str
                               access_token,  # type: str
                               fields=None,  # type: Optional[Union[str, List, Tuple, Set]]
                               folder="inbox",  # type: str
                               count=10,  # type: Optional[int]
                               limit=200,  # type: int,
                               return_json=False  # type: bool
                               ):
        if fields is None:
            fields = self.DEFAULT_CONVERSATION_FIELDS

        args = {
            "access_token": access_token,
            "fields": enf_comma_separated("fields", fields),
            "folder": folder,
            "limit": limit,
        }

        conversations = []
        next_page = None

        while True:
            next_page, data = self.page_by_next(
                target=page_id, resource="conversations",
                args=args, next_page=next_page
            )
            data = data.get("data", [])

            if return_json:
                conversations.extend(data)
            else:
                conversations.extend([PageConversation.new_from_json_dict(item) for item in data])

            if count is not None:
                conversations = conversations[:count]
                break
            if next_page is None:
                break
        return conversations