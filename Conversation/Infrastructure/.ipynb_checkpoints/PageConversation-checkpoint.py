from attr import attrs, attrib
from pyfacebook import Api, BaseModel
from typing import Dict, List, Optional, Union, Tuple, Set

@attrs
class PageConversation(BaseModel):
    """
    Refer: https://developers.facebook.com/docs/graph-api/reference/v6.0/conversation
    """

    id = attrib(default=None, type=Optional[str])
    link = attrib(default=None, type=Optional[str], repr=False)
    snippet = attrib(default=None, type=Optional[str], repr=False)
    updated_time = attrib(default=None, type=Optional[str])
    message_count = attrib(default=None, type=Optional[int])
    unread_count = attrib(default=None, type=Optional[int])
    participants = attrib(default=None, type=Optional[Dict])
    senders = attrib(default=None, type=Optional[Dict])
    can_reply = attrib(default=None, type=Optional[bool], repr=False)
    is_subscribed = attrib(default=None, type=Optional[bool], repr=False)

    def __attrs_post_init__(self):
        if self.participants is not None and isinstance(self.participants, dict):
            participants = self.participants.get("data", [])
            self.participants = [People.new_from_json_dict(par) for par in participants]
        if self.senders is not None and isinstance(self.senders, dict):
            senders = self.senders.get("data", [])
            self.senders = [People.new_from_json_dict(sender) for sender in senders]