from pyfacebook import Api, BaseModel
from attr import attrs, attrib
from typing import Dict, List, Optional, Union, Tuple, Set

class People(BaseModel):
    """
    Refer: https://developers.facebook.com/docs/graph-api/reference/v6.0/conversation
    """
    id = attrib(default=None, type=Optional[str])
    name = attrib(default=None, type=Optional[str])
    email = attrib(default=None, type=Optional[str], repr=False)