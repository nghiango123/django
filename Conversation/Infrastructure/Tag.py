from typing import Dict, List, Optional, Union, Tuple, Set

from attr import attrs, attrib
from pyfacebook import Api, BaseModel

class Tags(BaseModel):
    name = attrib(default = None, type = Optional[str])