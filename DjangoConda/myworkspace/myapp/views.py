from django.shortcuts import render
from rest_framework.response import Response
from rest_framework import status
from myapp.models import Post, Channel, ChannelCustomer, Conversation, Message, CustomerProfile
from myapp.serializers import PostSerializer, MessageSerializer
from rest_framework.decorators import api_view
from myapp.repositories.postRepository import PostRepository
from myapp.repositories.channelRepository import ChannelRepository
from myapp.repositories.conversationRepository import ConversationRepository
from myapp.repositories.messageRepository import MessageRepository
from myapp.repositories.customerProfileRepository import CustomerProfileRepository
from myapp.repositories.channelCustomerRepository import ChannelCustomerRepository
from myapp.repositories.albumRepository import AlbumRepository
from myapp.repositories.photoRepository import PhotoRepository

# Create your views here.

class PostView:
    @api_view(['GET'])
    def get_post_all(self):
        post = Post.objects.all()
        postSerializer = PostSerializer()
        data = postSerializer.serializer_data(post, many=True)
        return Response(data)

    @api_view(['GET'])
    def get_post_by_id(self, post_id):
        post = Post.objects.get(source_id=post_id)
        postSerializer = PostSerializer()
        data = postSerializer.serializer_data(post)
        return Response(post)

    @api_view(['GET'])
    def get_fields(request):
        fields = request.GET.getlist("param")
        post = Post.objects.all().values(*fields)
        return Response(post)

    @api_view(['GET', 'POST'])
    def insert(request):
        if request.method == 'GET':
            return Response(u"Insert Object")
        elif request.method == 'POST':
            serializer = PostSerializer(data=request.data)
            if serializer.is_valid():
                serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @api_view(['GET', 'POST'])
    def insert_from_mongdb(self):
        post = PostRepository()
        post.insert_from_mongo()
        return Response("Insert successfully status:201")

    @api_view(['GET', 'POST'])
    def update_album_id_from_mongo(self):
        post = PostRepository()
        post.post_update_album_id()
        return Response("Update successfully status: 203")


class ChannelView:
    @api_view(['GET', 'POST'])
    def insert_from_mongodb(self):
        channel = ChannelRepository()
        channel.insert_from_mongo()
        return Response("Insert successfully status: 201")


class CustomerProfileView:
    @api_view(['GET', 'POST'])
    def insert_from_mongodb(self):
        customerProfile = CustomerProfileRepository()
        customerProfile.insert_from_mongo()
        return Response("Insert successfully status:201")


class ChannelCustomerView:
    @api_view(['GET', 'POST'])
    def insert_from_mongodb(self):
        channelCustomer = ChannelCustomerRepository()
        channelCustomer.insert_from_mongo()
        return Response("Insert successfully status:201")


class ConversationView:
    @api_view(['GET', 'POST'])
    def insert_from_mongodb(self):
        conversation = ConversationRepository()
        conversation.insert_from_mongo()
        return Response("Insert successfully status:201")

    @api_view(['GET', 'POST'])
    def update_last_first_time(self):
        conversation = ConversationRepository()
        data = conversation.update_last_first_time()
        return Response('Update successfully status: 204')


class MessageView:
    @api_view(['GET', 'POST'])
    def insert_from_mongodb(self):
        message = MessageRepository()
        message.insert_from_mongo()
        return Response("Insert successfully status:201")

    @api_view(['GET'])
    def get_message_all(self):
        message = Message.objects.all()[0:1000]
        messageSerializer = MessageSerializer()
        data = messageSerializer.serializer_data(message, many=True)
        return Response(data)

    @api_view(['GET'])
    def get_fields(request):
        fields = request.GET.getlist("param")
        message = Message.objects.all().values(*fields)[0:1000]
        return Response(message)
    @api_view(['GET'])
    def get_message_user(request):
        message = Message.objects.filter(sender='user').values('text', 'sender')[0:1000]
        return Response(message)
    @api_view(['GET'])
    def get_message_shop(request):
        message = Message.objects.filter(sender='shop').values('text', 'sender')[0:1000]
        return Response(message)

class AlbumView:
    @api_view(['GET', 'POST'])
    def insert_from_mongodb(self):
        album = AlbumRepository()
        album.insert_from_mongo()
        return Response("Insert sucessfully status: 201")

class PhotoView:
    @api_view(['GET', 'POST'])
    def insert_from_mongodb(self):
        photo = PhotoRepository()
        photo.insert_from_mongo()
        return Response("Insert successfully status: 201")