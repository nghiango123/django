from myapp.mongodb import MongodbData
from myapp.models import Channel, ChannelCustomer, Conversation, Message
from myapp.serializers import PostSerializer
from django.core.exceptions import ObjectDoesNotExist
from django.db.models.aggregates import Max, Min

from datetime import datetime
import uuid


class ConversationRepository:
    def __init__(self):
        self.mongo = MongodbData('192.168.20.107', '27078', 'facebook')

    def insert_from_mongo(self):
        user_inbox = self.mongo.data_user_name_file()
        data_conversation = self.mongo.data_conversation('fb_chatlog')
        list_channel_not_found = []
        list_conversation_exist = []
        list_conversation_not_channel_customer = []

        for shop in data_conversation:
            try:
                channel = Channel.objects.get(raw_id=shop)
                for obj in data_conversation[shop]:
                    try:
                        conversation = Conversation.objects.get(raw_id=obj['id'])
                        list_conversation_exist.append(obj['link'])
                    except ObjectDoesNotExist:
                        conversation = Conversation()
                        link = obj['link']
                        raw_id = obj['id']
                        name = user_inbox[link]['name']
                        conversation.id = uuid.uuid4()
                        conversation.name = name
                        conversation.link = link
                        conversation.raw_id = raw_id
                        conversation.db_created_at = datetime.now()
                        conversation.db_updated_at = datetime.now()
                        conversation.channel = channel

                        user_id = user_inbox[link]['id']
                        try:
                            channelCustomer = ChannelCustomer.objects.get(app_page_user_id=user_id)
                            conversation.channel_customer = channelCustomer
                        except ObjectDoesNotExist:
                            list_conversation_not_channel_customer.append(user_id)
                            continue
                        conversation.save()
            except ObjectDoesNotExist:
                list_channel_not_found.append(shop)

            with open('myapp/file_log/conversation/list_conversation_not_channel_customer.txt', 'w') as f:
                for ob in list_conversation_not_channel_customer:
                    f.write(ob)
                    f.write('\n')

    def update_last_first_time(self):
        last_message_time = {}
        first_message_time = {}

        data_last_time = Message.objects.all().values('conversation_id').annotate(time=Max('created_time'))
        data_first_time = Message.objects.all().values('conversation_id').annotate(time=Min('created_time'))

        for ob in data_last_time:
            last_message_time[ob['conversation_id']] = ob['time']
        for ob in data_first_time:
            first_message_time[ob['conversation_id']] = ob['time']

        for ob in data_last_time:
            converstion = Conversation.objects.get(pk=ob['conversation_id'])
            converstion.last_message_time = last_message_time[ob['conversation_id']]
            converstion.first_message_time = first_message_time[ob['conversation_id']]
            converstion.save()
