from myapp.mongodb import MongodbData
from myapp.models import Post, Channel, ChannelCustomer, Conversation, Message
from myapp.serializers import PostSerializer
from django.core.exceptions import ObjectDoesNotExist

from datetime import datetime
import uuid

class MessageRepository:
    def __init__(self):
        self.mongo = MongodbData('192.168.20.107', '27078', 'facebook')

    def insert_from_mongo(self):
        data_message = self.mongo.data_message('fb_chatlog')
        list_conversation_not_exist = []
        list_message_exist = []

        for obj in data_message:
            try:
                converstion = Conversation.objects.get(raw_id=obj['fb_conversation_id'])
                try:
                    message = Message.objects.get(raw_id=obj['message_id'])
                    list_message_exist.append((obj['message_id']))
                except ObjectDoesNotExist:
                    message = Message()
                    message.id = uuid.uuid4()
                    if obj['shop_id'] == obj['sender_id']:
                        message.sender = 'shop'
                        message.text = obj['shop_message']
                    else:
                        message.sender = 'user'
                        message.text = obj['user_message']
                    message.raw_id = obj['message_id']
                    message.created_time = obj['created_at']
                    message.db_updated_at = datetime.now()
                    message.db_created_at = datetime.now()
                    message.attachments = obj['attachments']
                    message.conversation = converstion
                    message.source_type = "facebook"
                    message.save()

            except ObjectDoesNotExist:
                list_conversation_not_exist.append(obj['link'])

        with open('myapp/file_log/message/list_message_exist.txt', 'w') as f:
            for ob in list_message_exist:
                f.write(ob)
                f.write('\n')
        with open('myapp/file_log/message/list_conversation_not_exist.txt', 'w') as f:
            for ob in list_conversation_not_exist:
                f.write(ob)
                f.write('\n')
