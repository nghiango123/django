from myapp.mongodb import MongodbData
from myapp.models import Album, Channel
from myapp.serializers import PostSerializer
from django.core.exceptions import ObjectDoesNotExist

from datetime import datetime
import uuid


class AlbumRepository:
    def __init__(self):
        self.mongo = MongodbData('192.168.20.107', '27078', 'facebook')

    def insert_from_mongo(self):
        data_album = self.mongo.data_album('fb_albums')
        list_channel_not_exist = []
        for page_id in data_album:
            try:
                channel = Channel.objects.get(raw_id=page_id)
                for ob in data_album[page_id]:
                    album = Album()
                    album.channel = channel
                    try:
                        obj = Album.objects.get(raw_id=ob['id'])
                    except ObjectDoesNotExist:
                        album.id = uuid.uuid4()
                        album.name = ob['name']
                        album.raw_id = ob['id']
                        album.description = ob['description']
                        album.link = ob['link']
                        album.type = ob['type']
                        album.media_count = ob['count']
                        album.event = ob['event']
                        album.location = ob['location']
                        album.created_time = ob['created_time']
                        album.updated_time = ob['updated_time']
                        album.db_updated_at = datetime.now()
                        album.db_created_at = datetime.now()
                        album.save()
            except ObjectDoesNotExist:
                list_channel_not_exist.append(page_id)
        with open('myapp/file_log/album/list_channel_not_exist.txt', 'w') as f:
            for ob in list_channel_not_exist:
                f.write(ob)
                f.write('\n')
