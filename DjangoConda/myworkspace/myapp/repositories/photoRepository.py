from myapp.mongodb import MongodbData
from myapp.models import Post, Album, Photo
from myapp.serializers import PostSerializer
from django.core.exceptions import ObjectDoesNotExist

from datetime import datetime
import uuid


class PhotoRepository:
    def __init__(self):
        self.mongo = MongodbData('192.168.20.107', '27078', 'facebook')

    def insert_from_mongo(self):
        list_photo_exist = []
        list_photo_album_not_exist = []
        list_post_not_exist = []
        data_photo = self.mongo.data_photo('fb_album_photos')
        for ab in data_photo:
            for obj in data_photo[ab]:
                if obj['height'] == "":
                    obj['height'] = 0
                if obj['width'] == "":
                    obj['width'] = 0
                if obj['photo_created_time'] == "":
                    obj['photo_created_time'] = None
                try:
                    album = Album.objects.get(raw_id=obj['album_id'])
                    try:
                        photo = Photo.objects.get(raw_id=obj['id'])
                        list_photo_exist.append(obj['id'])
                    except ObjectDoesNotExist:
                        photo = Photo()
                        photo.id = uuid.uuid4()
                        photo.name = obj['album_name']
                        photo.link = obj['link']
                        photo.height = obj['height']
                        photo.width = obj['width']
                        photo.raw_id = obj['id']
                        photo.created_time = obj['photo_created_time']
                        try:
                            post = Post.objects.get(raw_id=obj['page_story_id'])
                            photo.post = post
                        except ObjectDoesNotExist:
                            list_post_not_exist.append(obj['page_story_id'])
                        photo.album = album
                        photo.db_created_at = datetime.now()
                        photo.db_updated_at = datetime.now()
                        photo.save()
                except ObjectDoesNotExist:
                    list_photo_album_not_exist.append(obj['id'])
        with open('myapp/file_log/photo/list_photo_album_not_exist.txt', 'w') as f:
            for ob in list_photo_album_not_exist:
                f.write(ob)
                f.write("\n")
        with open('myapp/file_log/photo/list_photo_exist.txt', 'w') as f:
            for ob in list_photo_exist:
                f.write(ob)
                f.write('\n')
        with open('myapp/file_log/photo/list_post_not_exist.txt', 'w') as f:
            for ob in list_post_not_exist:
                f.write(ob)
                f.write(('\n'))