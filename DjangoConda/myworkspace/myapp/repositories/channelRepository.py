from myapp.mongodb import MongodbData
from myapp.models import Post, Channel, ChannelCustomer, Conversation, Message
from myapp.serializers import PostSerializer
from django.core.exceptions import ObjectDoesNotExist

from datetime import datetime
import uuid


class ChannelRepository:
    def __init__(self):
        self.mongo = MongodbData('192.168.20.107', '27078', 'facebook')

    def insert_from_mongo(self):
        list_channel_exist = []
        data_channel = self.mongo.data_channel('fb_post_full')
        for ob in data_channel:
            try:
                channel = Channel.objects.get(raw_id=ob['id'])
                list_channel_exist.append(ob['id'])
            except:
                channel = Channel()
                channel.id = uuid.uuid4()
                channel.name = ob['name']
                channel.type = "facebook"
                channel.raw_id = ob['id']
                channel.link = "https://www.facebook.com/" + str(ob['id'])
                channel.created_time = datetime.now()
                channel.db_created_at = datetime.now()
                channel.db_updated_at = datetime.now()
                channel.save()
        with open('myapp/file_log/channel/list_channel_exist_insert.txt', 'w') as f:
            for ob in list_channel_exist:
                f.write(ob)
                f.write('\n')
