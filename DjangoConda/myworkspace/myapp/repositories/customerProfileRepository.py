from myapp.mongodb import MongodbData
from myapp.models import CustomerProfile
from myapp.serializers import PostSerializer
from django.core.exceptions import ObjectDoesNotExist

from datetime import datetime
import uuid


class CustomerProfileRepository:
    def __init__(self):
        self.mongo = MongodbData('192.168.20.107', '27078', 'facebook')

    def insert_from_mongo(self):
        count = 0
        list_user_id_in_customerProfile = []
        user_inbox = self.mongo.data_user_name_file()
        for ln in user_inbox:
            customerProfile = CustomerProfile()
            customerProfile.id = uuid.uuid4()
            customerProfile.link = "https://www.facebook.com/" + str(user_inbox[ln]['id'])
            customerProfile.user_name = user_inbox[ln]['name']
            customerProfile.name = user_inbox[ln]['name']
            customerProfile.type = "facebook"
            customerProfile.db_created_at = datetime.now()
            customerProfile.db_updated_at = datetime.now()
            customerProfile.save()
            list_user_id_in_customerProfile.append([user_inbox[ln]['id'], customerProfile.id])
            count += 1
        with open('myapp/data_recycle/customer_profile/list_user_id_in_customerProfile.txt', 'w') as f:
            for ob in list_user_id_in_customerProfile:
                f.write(str(ob[0]))
                f.write(' ')
                f.write(str(ob[1]))
                f.write('\n')
