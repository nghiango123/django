from myapp.mongodb import MongodbData
from myapp.models import Post, Channel, ChannelCustomer, Conversation, Message
from myapp.serializers import PostSerializer
from django.core.exceptions import ObjectDoesNotExist

from datetime import datetime
import uuid

class PostRepository:
    def __init__(self):
        self.mongo = MongodbData('192.168.20.107', '27078', 'facebook')

    def insert_from_mongo(self):
        list_post_exist = []
        list_channel_not_exist = []
        list_post_not_attachments = []
        data_mongo = self.mongo.data_post('fb_post_full')

        word_key = u'phát trực tiếp'
        word_key_1 = u'https://www.facebook.com/media/set'

        for obj in data_mongo:
            if 'attachments_media_type_0' not in obj:
                list_post_not_attachments.append(obj['id'])
                obj['attachments_media_type_0'] = 'text'
            elif obj['attachments_media_type_0'] == '':
                obj['attachments_media_type_0'] = 'text'
            elif word_key in obj['story']:
                obj['attachments_media_type_0'] = 'livestream'
            elif obj['attachments_media_type_0'] == 'video' and word_key not in obj['story']:
                obj['attachments_media_type_0'] = 'video'
            elif obj['attachments_media_type_0'] == 'album' and word_key_1 not in obj['attachments_url_0']:
                obj['attachments_media_type_0'] = 'timeline album'
            elif obj['attachments_media_type_0'] == 'album' and word_key_1 in obj['attachments_url_0']:
                obj['attachments_media_type_0'] = 'album'

            if obj['shares'] == '':
                obj['shares'] = 0
            try:
                post = Post.objects.get(raw_id=obj['id'])
                list_post_exist.append(obj['id'])
            except ObjectDoesNotExist:
                post = Post()
                try:
                    channel = Channel.objects.get(raw_id=obj['page_id'])
                    post.id = uuid.uuid4()
                    post.type = obj['attachments_media_type_0']
                    post.link = obj['permalink_url']
                    post.message = obj['message']
                    post.story = obj['story']
                    post.share_count = obj['shares']
                    post.is_instagram_eligible = obj['is_instagram_eligible']
                    post.is_eligible_for_promation = obj['is_eligible_for_promotion']
                    post.is_hidden = obj['is_hidden']
                    post.is_popular = obj['is_popular']
                    post.is_spherical = obj['is_spherical']
                    post.channel = channel
                    post.raw_id = obj['id']
                    post.created_time = obj['created_time']
                    post.updated_time = obj['updated_time']
                    post.db_created_at = datetime.now()
                    post.db_updated_at = datetime.now()
                    res = post.save()
                    print(res)
                except ObjectDoesNotExist:
                    list_channel_not_exist.append(obj['shop_id'])
        with open('myapp/file_log/post/list_post_exist_insert.txt', 'w') as f:
            for ob in list_post_exist:
                f.write(ob)
                f.write('\n')
        with open('myapp/file_log/post/list_channel_not_exist_insert.txt', 'w') as f:
            for ob in list_channel_not_exist:
                f.write(ob)
                f.write('\n')
        with open('myapp/file_log/post/list_post_not_attachment.txt', 'w') as f:
            for ob in list_post_not_attachments:
                f.write(ob)
                f.write('\n')

    def post_update_album_id(self):
        list_post_not_found = []
        data_mongo = self.mongo.data_post_album_id('fb_album_photos')
        for post_id in data_mongo:
            try:
                post = Post.objects.get(raw_id=str(post_id))
                post.album = data_mongo[post_id]
                post.save()
            except:
                list_post_not_found.append(str(post_id))

        with open('myapp/file_log/post/list_post_not_found_album_id.txt', 'w') as f:
            for ob in list_post_not_found:
                f.write(ob)
                f.write('\n')
