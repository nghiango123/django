from myapp.mongodb import MongodbData
from myapp.models import Channel, ChannelCustomer, CustomerProfile
from myapp.serializers import PostSerializer
from django.core.exceptions import ObjectDoesNotExist

from datetime import datetime
import uuid


class ChannelCustomerRepository:
    def __init__(self):
        self.mongo = MongodbData('192.168.20.107', '27078', 'facebook')

    def insert_from_mongo(self):
        user_inbox = self.mongo.data_user_name_file()
        data_channel, list_user_id = self.mongo.data_channel_customer('fb_chatlog')
        list_channel_not_found = []
        list_link_not_in_user_inbox = []
        list_channel_customer_exist = []

        for shop in data_channel:
            try:
                channel = Channel.objects.get(raw_id=shop)
                for ln in data_channel[shop]:
                    if ln not in user_inbox:
                        list_link_not_in_user_inbox.append(ln)
                        continue
                    user_id = user_inbox[ln]['id']
                    user_name = user_inbox[ln]['name']
                    try:
                        channelCustomer = ChannelCustomer.objects.get(app_page_user_id=user_id)
                        list_channel_customer_exist.append(user_id)
                    except ObjectDoesNotExist:
                        channelCustomer = ChannelCustomer()
                        channelCustomer.id = uuid.uuid4()
                        channelCustomer.channel = channel
                        channelCustomer.app_page_user_id = user_id
                        channelCustomer.name = user_name
                        if user_id in list_user_id:
                            try:
                                customerProfile = CustomerProfile.objects.get(pk=list_user_id[user_id])
                                channelCustomer.customer_profile = customerProfile
                            except ObjectDoesNotExist:
                                None
                        channelCustomer.save()
            except ObjectDoesNotExist:
                list_channel_not_found.append(shop)

        with open('myapp/file_log/channel_customer/list_channel_not_found.txt', 'w') as f:
            for ob in list_channel_not_found:
                f.write(ob)
                f.write('\n')
        with open('myapp/file_log/channel_customer/list_link_not_in_user_inbox.txt', 'w') as f:
            for ob in list_link_not_in_user_inbox:
                f.write(ob)
                f.write('\n')
        with open('myapp/file_log/channel_customer/list_channel_customer_exist.txt', 'w') as f:
            for ob in list_channel_not_found:
                f.write(ob)
                f.write('\n')
