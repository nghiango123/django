from django.urls import path

from . import views

urlpatterns = [
    #     path('post', views.ListCreatePostView.as_view()),
    #     path('Post/<int:pk>', views.UpdateDeletePostView.as_view()),
    path('insert_many_post', views.PostView.insert_from_mongdb, name='Insert many Post'),
    path('insert_post', views.PostView.insert, name="Insert post"),
    path('get_post_by_id/<str:post_id>', views.PostView.get_post_by_id, name='get_by_post_id'),
    path('get_post', views.PostView.get_post_all, name='get_post_all'),
    path('get_post/', views.PostView.get_fields, name='Get id page'),

    path('insert_many_channel', views.ChannelView.insert_from_mongodb, name='insert_many'),
    path('insert_many_channel_customer', views.ChannelCustomerView.insert_from_mongodb, name='insert_many'),
    path('insert_many_conversation', views.ConversationView.insert_from_mongodb, name='insert_many'),
    path('insert_many_message', views.MessageView.insert_from_mongodb, name='insert_many'),
    path('insert_many_customer_profile', views.CustomerProfileView.insert_from_mongodb, name='insert_many'),
    path('insert_many_album', views.AlbumView.insert_from_mongodb, name='insert_many'),
    path('insert_many_photo', views.PhotoView.insert_from_mongodb, name='Insert_many'),

    path('update_time_conversation', views.ConversationView.update_last_first_time, name='update_time'),
    path('update_post_album_id', views.PostView.update_album_id_from_mongo, name='Update Album)id of Post'),

    path('get_message', views.MessageView.get_message_all, name='Get all message'),
    path('get_message/', views.MessageView.get_fields, name="Get message"),
    path('get_message_user', views.MessageView.get_message_user, name="Get message user"),
    path('get_message_shop', views.MessageView.get_message_shop, name="Get message shop"),
]
