from django.core.exceptions import ObjectDoesNotExist
from rest_framework import serializers
from .models import Channel, Post, Message
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
import io
from .mongodb import MongodbData


class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = '__all__'

    # def create(self, obj):
    #     try:
    #         ob = Post.objects.get(pk=obj.id)
    #         print('Post exists!')
    #     except ObjectDoesNotExist:
    #         post = obj
    #         post.save()
    #         print('Post insert successful!')
    def serializer_data(self, data, many=False):
        data_serializer = PostSerializer(data, many=many)
        return data_serializer.data

    def render_json(self, data):
        content = JSONRenderer().render(data)
        return content

    def deserializer_data(self, data):
        stream = io.BytesIO(data)
        data = JSONParser().parse(stream)
        return data


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = '__all__'

    def serializer_data(self, data, many=False):
        data_serializer = MessageSerializer(data, many=many)
        return data_serializer.data

    def render_json(self, data):
        content = JSONRenderer().render(data)
        return content

    def deserializer_data(self, data):
        stream = io.BytesIO(data)
        data = JSONParser().parse(stream)
        return data
