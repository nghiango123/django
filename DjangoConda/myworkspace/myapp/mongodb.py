from pymongo import MongoClient
import datetime


class ConnectManager:
    def __init__(self, host, port, database):
        self.port = port
        self.host = host
        self.database = database

    def __enter__(self):
        connect_string = "mongodb://" + self.host + ":" + self.port + "/" + self.database
        self.client = MongoClient(connect_string)
        print("Connect Database Successful!")
        return self.client

    def __exit__(self, type, value, traceback):
        print("Connection will be closed now!")
        self.client.close()
        return True


class MongodbData:
    def __init__(self, host, port, database):
        self.port = port
        self.host = host
        self.database = database

    def data_post(self, collection):
        list_post = []
        data = None

        with ConnectManager(self.host, self.port, self.database) as mongo:
            dbname = mongo[self.database]
            coll = dbname[collection]
            data = coll.find({})
        if data is None:
            print("Don't get data from MongoDB!!")
        else:
            for ob in data:
                list_post.append(ob)

        if len(list_post) == 0:
            print("No data")
            return
        return list_post

    def data_channel(self, collection):
        list_channel = []
        result = None
        with ConnectManager(self.host, self.port, self.database) as mongo:
            dbname = mongo[self.database]
            coll = dbname[collection]
            result = coll.aggregate([
                {
                    '$group': {
                        '_id': '$page_id',
                        'name': {
                            '$max': '$page_name'
                        }
                    }
                }
            ])
        if result is None:
            print("Don't get data from MongoDB!!")
        else:
            for ob in result:
                list_channel.append({'id': ob['_id'], 'name': ob['name']})
        return list_channel

    def data_message(self, collection):
        list_message = []
        data = None
        with ConnectManager(self.host, self.port, self.database) as mongo:
            dbname = mongo[self.database]
            coll = dbname[collection]
            data = coll.find({})
        if data is None:
            print("don't get data from MongoDB")
            return
        else:
            for ob in data:
                list_message.append(ob)
        if len(list_message) == 0:
            print("No data")
            return
        return list_message

    def data_user_name(self, collection):
        list_data_username = []
        data = None
        user_inbox = {}
        link_not_user_id = []
        link_not_user_name = []
        link_user_not_found = []
        with ConnectManager(self.host, self.port, self.database) as mongo:
            dbname = mongo[self.database]
            coll = dbname[collection]
            link_inbox = coll.distinct("link")
            for ln in link_inbox:
                user_id = None
                user_name = None
                filter = {
                    'link': ln
                }
                limit = 10
                result = coll.find(
                    filter=filter,
                    limit=limit
                )
                if result is None:
                    link_user_not_found.append(ln)
                    continue
                for ob in result:
                    if ob["shop_id"] == ob["sender_id"]:
                        user_id = ob['receiver_id']
                        continue
                    else:
                        user_id = ob["sender_id"]
                        user_name = ob["sender_name"]
                        break
                if user_name is None:
                    link_not_user_name.append(ln)
                if user_id is None:
                    link_not_user_id.append(ln)
                    continue
                user_inbox[ln] = {"id": user_id, "name": user_name}
        with open('myapp/data_recycle/customer_info/user_name_and_id.txt', 'w', encoding="utf-8")  as f:
            for ln in user_inbox:
                f.write(ln)
                f.write('**')
                f.write(user_inbox[ln]['id'])
                f.write('**')
                if user_inbox[ln]['name'] is not None:
                    f.write(user_inbox[ln]['name'])
                f.write('\n')
        with open('myapp/data_recycle/customer_info/link_not_user_name.txt', 'w') as f:
            for ln in link_not_user_name:
                f.write(ln)
                f.write('\n')
        return user_inbox

    def data_user_name_file(self):
        user_inbox = {}
        links = None
        link_not_user_name = []

        with open('myapp/data_recycle/customer_info/link_not_user_name.txt', 'r') as f:
            data = f.read()
            link_not_user_name = data.split('\n')
        with open('myapp/data_recycle/customer_info/user_name_and_id.txt', 'r', encoding="utf-8") as f:
            links = f.read()
        list_link = links.split('\n')
        for ln in list_link[:len(list_link) - 1]:
            tmp = ln.split('**')
            if tmp[0] not in link_not_user_name:
                user_inbox[tmp[0]] = {"id": tmp[1], "name": tmp[2]}
            else:
                user_inbox[tmp[0]] = {"id": tmp[1], "name": 'UNKOWN'}
        return user_inbox

    def data_channel_customer(self, collection):
        data_channel = {}
        list_user_id = {}
        link_not_user_name = []
        with ConnectManager(self.host, self.port, self.database) as mongo:
            dbname = mongo[self.database]
            coll = dbname[collection]
            result = coll.aggregate([
                {
                    '$group': {
                        '_id': '$link',
                        'shop': {
                            '$max': '$shop_id'
                        }
                    }
                }
            ])
            for ob in result:
                if ob['shop'] not in data_channel:
                    data_channel[ob['shop']] = []
                data_channel[ob['shop']].append(ob['_id'])

        with open('myapp/data_recycle/customer_profile/list_user_id_in_customerProfile.txt', 'r') as f:
            data = f.read()
            data = data.split('\n')
            for item in data:
                tmp = item.split(' ')
                if len(tmp) < 2:
                    continue
                if tmp[0] not in list_user_id:
                    list_user_id[tmp[0]] = tmp[1]
        return data_channel, list_user_id

    def data_conversation(self, collection):
        link_conversation_id = {}
        with ConnectManager(self.host, self.port, self.database) as mongo:
            dbname = mongo[self.database]
            coll = dbname[collection]
            result = coll.aggregate([
                {
                    '$group': {
                        '_id': '$link',
                        'conversation_id': {
                            '$max': '$fb_conversation_id'
                        },
                        'shop': {
                            '$max': '$shop_id'
                        }
                    }
                }
            ])
            for ob in result:
                if ob['shop'] not in link_conversation_id:
                    link_conversation_id[ob['shop']] = []
                link_conversation_id[ob['shop']].append({'link': ob['_id'], 'id': ob['conversation_id']})

        return link_conversation_id

    def data_album(self, collection):
        list_data_album = {}
        with ConnectManager(self.host, self.port, self.database) as mongo:
            dbname = mongo[self.database]
            coll = dbname[collection]
            data_mongo = coll.find({})
            for ob in data_mongo:
                if ob['page_id'] not in list_data_album:
                    list_data_album[ob['page_id']] = []
                list_data_album[ob['page_id']].append(ob)
        return list_data_album

    def data_photo(self, collection):
        list_data_album = {}
        with ConnectManager(self.host, self.port, self.database) as mongo:
            dbname = mongo[self.database]
            coll = dbname[collection]
            data_mongo = coll.find({})
            for ob in data_mongo:
                if ob['page_story_id'] is None:
                    ob['page_story_id'] = "unknow"
                if ob['album_id'] not in list_data_album:
                    list_data_album[ob['album_id']] = []
                # if ob['page_story_id'] not in list_data_album[ob['album_id']]:
                #     list_data_album[ob['album_id']][ob['page_story_id']] = []
                list_data_album[ob['album_id']].append(ob)

        return list_data_album

    def data_post_album_id(self, collection):
        list_data_post = {}
        with ConnectManager(self.host, self.port, self.database) as mongo:
            dbname = mongo[self.database]
            coll = dbname[collection]
            result = coll.aggregate([
                {
                    '$group': {
                        '_id': '$page_story_id',
                        'album': {
                            '$max': '$album_id'
                        }
                    }
                }
            ])
        for ob in result:
            list_data_post[ob['_id']] = ob['album']

        return list_data_post



