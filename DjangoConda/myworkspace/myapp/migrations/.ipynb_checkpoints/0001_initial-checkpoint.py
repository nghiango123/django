# Generated by Django 2.2.5 on 2021-07-22 04:04

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Channel',
            fields=[
                ('deleted', models.DateTimeField(blank=True, null=True)),
                ('created_at', models.DateTimeField()),
                ('updated_at', models.DateTimeField()),
                ('id', models.UUIDField(primary_key=True, serialize=False)),
                ('name', models.CharField(blank=True, max_length=100, null=True)),
                ('type', models.CharField(blank=True, max_length=100, null=True)),
                ('source_id', models.CharField(max_length=100)),
                ('access_token', models.CharField(blank=True, max_length=1000, null=True)),
                ('token_expired_datetime', models.DateTimeField(blank=True, null=True)),
                ('avatar_url', models.CharField(blank=True, max_length=1000, null=True)),
                ('url', models.CharField(blank=True, max_length=1000, null=True)),
                ('is_active', models.BooleanField()),
                ('shop_id', models.UUIDField(blank=True, null=True)),
            ],
            options={
                'db_table': 'channel',
                'managed': True,
                'unique_together': {('type', 'source_id')},
            },
        ),
        migrations.CreateModel(
            name='CustomerInfo',
            fields=[
                ('deleted', models.DateTimeField(blank=True, null=True)),
                ('created_at', models.DateTimeField()),
                ('updated_at', models.DateTimeField()),
                ('id', models.UUIDField(primary_key=True, serialize=False)),
                ('name', models.CharField(blank=True, max_length=100, null=True)),
                ('phone', models.CharField(blank=True, max_length=20, null=True)),
                ('email', models.CharField(blank=True, max_length=100, null=True)),
                ('address', models.CharField(blank=True, max_length=100, null=True)),
                ('city', models.SmallIntegerField(blank=True, null=True)),
                ('district', models.SmallIntegerField(blank=True, null=True)),
                ('ward', models.SmallIntegerField(blank=True, null=True)),
                ('gender', models.CharField(max_length=20)),
                ('nationality', models.CharField(blank=True, max_length=100, null=True)),
            ],
            options={
                'db_table': 'customer_info',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Post',
            fields=[
                ('deleted', models.DateTimeField(blank=True, null=True)),
                ('created_at', models.DateTimeField()),
                ('updated_at', models.DateTimeField()),
                ('id', models.UUIDField(primary_key=True, serialize=False)),
                ('type', models.CharField(default='NORMAL', max_length=100)),
                ('live_status', models.CharField(blank=True, max_length=100, null=True)),
                ('message', models.TextField(blank=True, null=True)),
                ('source_created_time', models.DateTimeField(blank=True, null=True)),
                ('source_id', models.CharField(max_length=100, unique=True)),
                ('source_url', models.CharField(max_length=1000)),
                ('medias', models.TextField(blank=True, null=True)),
                ('media_type', models.CharField(blank=True, max_length=100, null=True)),
                ('thumbnail_url', models.CharField(blank=True, max_length=1000, null=True)),
                ('title', models.CharField(blank=True, max_length=2000, null=True)),
                ('is_use', models.BooleanField(default=True)),
                ('skipped', models.BooleanField(default=False)),
                ('reply_when_have_phone', models.BooleanField(default=True)),
                ('reply_when_have_key_words', models.BooleanField(default=True)),
                ('key_words', models.TextField(blank=True, null=True)),
                ('live_end_at', models.DateTimeField(blank=True, null=True)),
                ('extra_data', models.TextField(blank=True, null=True)),
                ('selected', models.BooleanField(default=True)),
                ('total_likes', models.BigIntegerField()),
                ('total_video_views_unique', models.BigIntegerField(blank=True, null=True)),
                ('auto_hide_comment', models.BooleanField(default=False)),
                ('type_data_crawl', models.CharField(blank=True, default='Webhook', max_length=100, null=True)),
                ('channel', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='myapp.Channel')),
            ],
            options={
                'db_table': 'post',
                'managed': True,
            },
        ),
    ]
