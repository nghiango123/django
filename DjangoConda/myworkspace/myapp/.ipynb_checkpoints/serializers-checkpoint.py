from django.core.exceptions import ObjectDoesNotExist
from rest_framework import serializers
from .models import CustomerInfo, Channel, Post
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
import io
from .mongodb import MongodbData

class CustomerInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomerInfo
        fields = '__all__'
        
    def serializer_data(self, data):
        data_serializer = CustomerInfoSerializer(data)
        return data_serializer.data
    def render_json(self, data):
        content = JSONRenderer().render(data)
        return content
    def deserializer_data(self, data):
        stream = io.BytesIO(data)
        data = JSONParser().parse(stream)
        return data

    
class ChannelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Channel
        fields = '__all__'

    def serializer_data(self, data):
        data_serializer = ChannelSerializer(data)
        return data_serializer.data
    def render_json(self, data):
        content = JSONRenderer().render(data)
        return content
    def deserializer_data(self, data):
        stream = io.BytesIO(data)
        data = JSONParser().parse(stream)
        return data

class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = '__all__'

    def create(self, obj):
        try:
            ob = Post.objects.get(pk=obj.id)
            print('Post exists!')
        except ObjectDoesNotExist:
            post = obj
            post.save()
            print('Post insert successful!')
    def serializer_data(self, data, many = False):
        data_serializer = PostSerializer(data, many = many)
        return data_serializer.data
    def render_json(self, data):
        content = JSONRenderer().render(data)
        return content
    def deserializer_data(self, data):
        stream = io.BytesIO(data)
        data = JSONParser().parse(stream)
        return data