from django.urls import path

from . import views

urlpatterns = [
#     path('post', views.ListCreatePostView.as_view()),
#     path('Post/<int:pk>', views.UpdateDeletePostView.as_view()),
    path('insert_many_post', views.PostView.insert_from_mongdb, name = 'Insert many Post'),
    path('insert_post', views.PostView.insert, name = "Insert post"),
    path('get_by_post_id/<str:post_id>', views.PostView.get_by_post_id, name = 'get_by_post_id'),
    path('get_post', views.PostView.get_post_all, name = 'get_post_all'),
    path('insert_many_channel', views.ChannelView.insert_from_mongodb, name = 'insert_many'),
    path('get_post/', views.PostView.get_fields, name = 'Get id page'),
    
    path('insert_many_customer', views.CustomerInfoView.insert_from_mongodb, name = 'Insert many customer_info'),
]