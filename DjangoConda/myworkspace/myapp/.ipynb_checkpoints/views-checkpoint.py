from django.shortcuts import render
from rest_framework.response import Response
from rest_framework import status
from myapp.models import Post, Channel, CustomerInfo
from myapp.serializers import PostSerializer
from django.core.exceptions import ObjectDoesNotExist

from rest_framework.decorators import api_view
from myapp.mongodb import MongodbData

import uuid
from datetime import datetime

# Create your views here.

class PostView:   
    @api_view(['GET'])
    def get_post_all(self):
        post = Post.objects.all()
        postSerializer = PostSerializer()
        data = postSerializer.serializer_data(post, many = True)
        return Response(data)
    
    @api_view(['GET'])
    def get_by_post_id(self, post_id):
        post = Post.objects.get(source_id = post_id)
        postSerializer = PostSerializer()
        data = postSerializer.serializer_data(post)
        return Response(post)
    
    @api_view(['GET'])
    def get_fields(request):
        fields = request.GET.getlist("param")
        qs = Post.objects.all()
        post = qs.values(*fields)
        return Response(post)
    @api_view(['GET', 'POST'])
    def insert(request):
        if request.method == 'GET':
            return Response(u"Insert Object")
        elif request.method == 'POST':
            serializer = PostSerializer(data=request.data)
            if serializer.is_valid():
                serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)      
    @api_view(['GET', 'POST'])
    def insert_from_mongdb(self):
        list_post_exist = []
        list_channel_not_exist = []
        livestream = []
        mongo = MongodbData('192.168.20.107', '27078', 'facebook')
        data_mongo = mongo.data_post('fb_post')
        for ob in data_mongo:
            if ob['story'] != "" and ob['media_type'] == 'video':
                livestream.append(ob)
        for obj in data_mongo:
            try:
                post = Post.objects.get(source_id = obj['post_id'])
                list_post_exist.append(obj['post_id'])
            except ObjectDoesNotExist:
                post = Post()
                try:
                    channel = Channel.objects.get(source_id = obj['shop_id'])
                    post.id = uuid.uuid4()
                    if obj in livestream:
                        post.type = "LIVE_STREAM"
                    else:
                        post.type = 'NORMAL'
                    post.message = obj['message']
                    post.source_created_time = obj['created_time']
                    post.source_id = obj['post_id']
                    post.source_url = obj['permalink_url']
                    post.media_type = obj['media_type']
                    post.created_at = datetime.now()
                    post.updated_at = datetime.now()
                    post.total_likes = obj['post_reactions_by_type_total']
                    post.total_video_views_unique = obj['post_video_views_unique']
                    post.channel = channel
                    post.type_data_crawl = 'Graph API'
                    res = post.save()
                    print(res)
                except ObjectDoesNotExist:    
                    list_channel_not_exist.append(obj['shop_id'])
        with open('myapp/file_log/post/list_post_exist_insert.txt', 'w') as f:
            for ob in list_post_exist:
                f.write(ob)
                f.write('\n')
        with open('myapp/file_log/post/list_channel_not_exist_insert.txt', 'w') as f:
            for ob in list_channel_not_exist:
                f.write(ob)
                f.write('\n')
        return Response("Insert successfully status:201")
    @api_view(['PUT'])
    def update_from_mongodb(self):  
        list_post_id_not_found = []
        mongo = MongodbData('192.168.20.107', '27078', 'facebook')
        data_post = mongo.data_post('fb_post')
        
        for obj in data_post:
            try:
                post = Post.objects.get(source_id = obj['post_id'])
                try:
                    channel = Channel.objects.get(source_id = obj['shop_id'])
                    post.id = uuid.uuid4()
                    post.type = "NORMAl"
                    post.message = obj['message']
                    post.source_created_time = obj['created_time']
                    post.source_id = obj['post_id']
                    post.source_url = obj['permalink_url']
                    post.media_type = obj['media_type']
                    post.created_at = datetime.now()
                    post.updated_at = datetime.now()
                    post.total_likes = obj['post_reactions_by_type_total']
                    post.total_video_views_unique = obj['post_video_views_unique']
                    post.channel = channel
                    post.type_data_crawl = 'Graph API'
                    post.save()
                except ObjectDoesNotExist:
                    print("Error shop_id not found")
                    continue
            except ObjectDoesNotExist:
                list_post_id_not_found.append(obj['post_id'])
        with open('myapp/file_log/post/list_post_id_not_found_update.txt', 'w') as f:
            for ob in list_post_id_not_found:
                f.write(ob)
                f.write('\n')
        return Response("Update successfully status: 204")

class ChannelView:
    @api_view(['GET', 'POST'])
    def insert_from_mongodb(self):    
        list_channel_exist = []
        mongo = MongodbData('192.168.20.107', '27078', 'facebook')
        data_channel = mongo.data_channel('fb_chatlog')
        
        for shop_id in data_channel:
            try:
                channel = Channel.objects.get(source_id = shop_id)
                list_channel_exist.append(shop_id)
            except:
                channel = Channel()
                channel.id = uuid.uuid4()
                channel.type = "FACEBOOK"
                channel.source_id = shop_id
                channel.url = "https://www.facebook.com/" + str(shop_id)
                channel.created_at = datetime.now()
                channel.updated_at = datetime.now()
                channel.save()
        with open('myapp/file_log/channel/list_channel_exist_insert.txt', 'w') as f:
            for ob in list_channel_exist:
                f.write(ob)
                f.write('\n')
        return Response("Insert successfully status: 201")

class CustomerInfoView:
    
    @api_view(['GET', 'POST'])
    def insert_from_mongodb(self):
        count = 0
        list_user_id_in_customerInfor = []
        mongo = MongodbData('192.168.20.107', '27078', 'facebook')
        user_inbox = mongo.data_user_name_file()
        for ln in user_inbox:
            if user_inbox[ln]['name'] != 'UNKOWN':
                customerInfo = CustomerInfo()
                customerInfo.id = uuid.uuid4()
                customerInfo.name = user_inbox[ln]['name']
                customerInfo.created_at = datetime.now()
                customerInfo.updated_at = datetime.now()
                customerInfo.data_source = 'Graph API'                
                customerInfo.save()
                list_user_id_in_customerInfor.append([user_inbox[ln]['id'], customerInfo.id])        
                count += 1
        with open('myapp/data_recycle/customer_info/list_user_id_in_customerInfor.txt', 'w') as f:
            for ob in list_user_id_in_customerInfor:
                f.write(str(ob[0]))
                f.write(' ')
                f.write(str(ob[1]))
                f.write('\n')
        return Response("Insert successfully status:201")

class ChannelCustomerView:
    @api_view(['GET', 'POST'])
    def insert_from_mongodb(self):
        mongo = MongodbData('192.168.20.107', '27078', 'facebook')
        user_inbox = mongo.data_user_name_file()        
        data_channel, list_user_id = mongo.data_channel_customer()
        list_channel_not_found = []
        
        for shop in data_channel:
            try:
                channel = Channel.objects.get(source_id = shop)
                for ln in data_channel[shop]:
                    if ln not in user_inbox:
                        continue
                    if user_inbox[ln]['name'] != 'UNKOWN':
                        user_id = user_inbox[ln]['id']
                        channelCustomer = ChannelCustomer()
            except:
                list_channel_not_found.append(shop)
                
            
                
                

                
                
                
        