from django.db import models

# Create models

class Common(models.Model):
    deleted = models.DateTimeField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    data_source = models.CharField(max_length=100, blank=True, null=True, default='Webhook')

    class Meta:
        abstract = True


class CustomerInfo(Common):
    id = models.UUIDField(primary_key=True)
    name = models.CharField(max_length=100, blank=True, null=True)
    phone = models.CharField(max_length=20, blank=True, null=True)
    email = models.CharField(max_length=100, blank=True, null=True)
    address = models.CharField(max_length=100, blank=True, null=True)
    city = models.SmallIntegerField(blank=True, null=True)
    district = models.SmallIntegerField(blank=True, null=True)
    ward = models.SmallIntegerField(blank=True, null=True)
    gender = models.CharField(max_length=20,default="UNKNOW")
    nationality = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'customer_info'

    def __str__(self):
        return self.id


class Channel(Common):
    id = models.UUIDField(primary_key=True)
    name = models.CharField(max_length=100, blank=True, null=True)
    type = models.CharField(max_length=100, blank=True, null=True)
    source_id = models.CharField(max_length=100)
    access_token = models.CharField(max_length=1000, blank=True, null=True)
    token_expired_datetime = models.DateTimeField(blank=True, null=True)
    avatar_url = models.CharField(max_length=1000, blank=True, null=True)
    url = models.CharField(max_length=1000, blank=True, null=True)
    is_active = models.BooleanField()
    shop_id = models.UUIDField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'channel'
        unique_together = (('type', 'source_id'),)

    def __str__(self):
        return self.id


class Post(Common):
    id = models.UUIDField(primary_key=True)
    type = models.CharField(max_length=100, default='NORMAL')
    live_status = models.CharField(max_length=100, blank=True, null=True)
    message = models.TextField(blank=True, null=True)
    source_created_time = models.DateTimeField(blank=True, null=True)
    source_id = models.CharField(unique=True, max_length=100)
    source_url = models.CharField(max_length=1000)
    medias = models.TextField(blank=True, null=True)
    media_type = models.CharField(max_length=100, blank=True, null=True)
    thumbnail_url = models.CharField(max_length=1000, blank=True, null=True)
    title = models.CharField(max_length=2000, blank=True, null=True)
    is_use = models.BooleanField(default=True)
    skipped = models.BooleanField(default=False)
    reply_when_have_phone = models.BooleanField(default=True)
    reply_when_have_key_words = models.BooleanField(default=True)
    key_words = models.TextField(blank=True, null=True)  # This field type is a guess.
    live_end_at = models.DateTimeField(blank=True, null=True)
    channel = models.ForeignKey(Channel, on_delete=models.CASCADE)
    extra_data = models.TextField(blank=True, null=True)
    selected = models.BooleanField(default=True)
    total_likes = models.BigIntegerField()
    total_video_views_unique = models.BigIntegerField(blank=True, null=True)
    auto_hide_comment = models.BooleanField(default=False)

    class Meta:
        managed = True
        db_table = 'post'

    def __str__(self):
        return self.id

class ChannelCustomer(Common):
    id = models.UUIDField(primary_key=True)
    source_id = models.CharField(max_length=100, blank=True, null=True)
    avatar_url = models.CharField(max_length=1000, blank=True, null=True)
    note = models.TextField(blank=True, null=True)
    channel = models.ForeignKey(Channel, on_delete=models.CASCADE)
    customer_info = models.ForeignKey(CustomerInfo, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'channel_customer'
        
    def __str__(self):
        return self.id