from django.db import models


class Shop(models.Model):
    id = models.UUIDField(primary_key=True)
    name = models.CharField(max_length=200, blank=True, null=True)
    domain = models.CharField(max_length=200, blank=True, null=True)
    db_created_at = models.DateTimeField(blank=True, null=True)
    db_updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'shop'


class Channel(models.Model):
    id = models.UUIDField(primary_key=True)
    name = models.CharField(max_length=500, blank=True, null=True)
    type = models.CharField(max_length=500, blank=True, null=True)
    raw_id = models.CharField(max_length=500, blank=True, null=True)
    link = models.CharField(max_length=500, blank=True, null=True)
    access_token = models.CharField(max_length=500, blank=True, null=True)
    token_expried_time = models.DateTimeField(blank=True, null=True)
    created_time = models.DateTimeField(blank=True, null=True)
    db_created_at = models.DateTimeField(blank=True, null=True)
    db_updated_at = models.DateTimeField(blank=True, null=True)
    shop = models.ForeignKey(Shop, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'channel'


class CustomerProfile(models.Model):
    id = models.UUIDField(primary_key=True)
    user_name = models.CharField(max_length=500, blank=True, null=True)
    name = models.CharField(max_length=500, blank=True, null=True)
    link = models.CharField(max_length=500, blank=True, null=True)
    type = models.CharField(max_length=500, blank=True, null=True)
    phone_number = models.CharField(max_length=500, blank=True, null=True)
    db_created_at = models.DateTimeField(blank=True, null=True)
    db_updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'customer_profile'


class Album(models.Model):
    id = models.UUIDField(primary_key=True)
    name = models.CharField(max_length=500, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    link = models.CharField(max_length=500, blank=True, null=True)
    type = models.CharField(max_length=500, blank=True, null=True)
    media_count = models.BigIntegerField(blank=True, null=True)
    location = models.CharField(max_length=500, blank=True, null=True)
    raw_id = models.CharField(max_length=500, blank=True, null=True)
    created_time = models.DateTimeField(blank=True, null=True)
    updated_time = models.DateTimeField(blank=True, null=True)
    db_created_at = models.DateTimeField(blank=True, null=True)
    db_updated_at = models.DateTimeField(blank=True, null=True)
    channel = models.ForeignKey(Channel, on_delete=models.CASCADE, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'album'


class ChannelCustomer(models.Model):
    id = models.UUIDField(primary_key=True)
    name = models.CharField(max_length=500, blank=True, null=True)
    fb_uid = models.CharField(max_length=500, blank=True, null=True)
    app_page_user_id = models.CharField(max_length=500, blank=True, null=True)
    channel = models.ForeignKey(Channel, models.DO_NOTHING, blank=True, null=True)
    customer_profile = models.ForeignKey(CustomerProfile, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'channel_customer'


class Conversation(models.Model):
    id = models.UUIDField(primary_key=True)
    name = models.CharField(max_length=500, blank=True, null=True)
    link = models.CharField(max_length=500, blank=True, null=True)
    raw_id = models.CharField(max_length=500, blank=True, null=True)
    first_message_time = models.DateTimeField(blank=True, null=True)
    last_message_time = models.DateTimeField(blank=True, null=True)
    db_created_at = models.DateTimeField(blank=True, null=True)
    db_updated_at = models.DateTimeField(blank=True, null=True)
    channel = models.ForeignKey(Channel, models.DO_NOTHING, blank=True, null=True)
    channel_customer = models.ForeignKey(ChannelCustomer, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'conversation'


class Post(models.Model):
    id = models.UUIDField(primary_key=True)
    message = models.TextField(blank=True, null=True)
    type = models.CharField(max_length=500, blank=True, null=True)
    story = models.CharField(max_length=500, blank=True, null=True)
    link = models.CharField(max_length=500, blank=True, null=True)
    share_count = models.BigIntegerField(blank=True, null=True)
    is_instagram_eligible = models.BooleanField(blank=True, null=True)
    is_eligible_for_promation = models.BooleanField(blank=True, null=True)
    is_hidden = models.BooleanField(blank=True, null=True)
    is_popular = models.BooleanField(blank=True, null=True)
    is_spherical = models.BooleanField(blank=True, null=True)
    raw_id = models.CharField(max_length=500, blank=True, null=True)
    created_time = models.DateTimeField(blank=True, null=True)
    updated_time = models.DateTimeField(blank=True, null=True)
    db_created_at = models.DateTimeField(blank=True, null=True)
    db_updated_at = models.DateTimeField(blank=True, null=True)
    album = models.ForeignKey(Album, models.DO_NOTHING, blank=True, null=True)
    channel = models.ForeignKey(Channel, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'post'


class Photo(models.Model):
    id = models.UUIDField(primary_key=True)
    name = models.CharField(max_length=500, blank=True, null=True)
    link = models.CharField(max_length=500, blank=True, null=True)
    width = models.BigIntegerField(blank=True, null=True)
    height = models.BigIntegerField(blank=True, null=True)
    raw_id = models.CharField(max_length=500)
    created_time = models.DateTimeField(blank=True, null=True)
    db_created_at = models.DateTimeField(blank=True, null=True)
    db_updated_at = models.DateTimeField(blank=True, null=True)
    post = models.ForeignKey(Post, models.DO_NOTHING, blank=True, null=True)
    channel = models.ForeignKey(Channel, models.DO_NOTHING, blank=True, null=True)
    album = models.ForeignKey(Album, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'photo'


class Comment(models.Model):
    id = models.UUIDField(primary_key=True)
    raw_id = models.CharField(max_length=500, blank=True, null=True)
    text = models.TextField(blank=True, null=True)
    attachments = models.JSONField(blank=True, null=True)
    like_count = models.BigIntegerField(blank=True, null=True)
    comment_count = models.BigIntegerField(blank=True, null=True)
    link = models.CharField(max_length=500, blank=True, null=True)
    created_time = models.DateTimeField(blank=True, null=True)
    db_created_at = models.DateTimeField(blank=True, null=True)
    db_updated_at = models.DateTimeField(blank=True, null=True)
    parent = models.ForeignKey('self', models.DO_NOTHING, blank=True, null=True)
    channel_customer = models.ForeignKey(ChannelCustomer, models.DO_NOTHING, blank=True, null=True)
    tagged_customer = models.ForeignKey(ChannelCustomer, models.DO_NOTHING, blank=True, null=True, related_name='+')
    conversation_id = models.UUIDField(blank=True, null=True)
    post = models.ForeignKey(Post, models.DO_NOTHING, blank=True, null=True)
    album = models.ForeignKey(Album, models.DO_NOTHING, blank=True, null=True)
    photo = models.ForeignKey(Photo, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'comment'


class Message(models.Model):
    id = models.UUIDField(primary_key=True)
    text = models.TextField(blank=True, null=True)
    sender = models.CharField(max_length=100, blank=True, null=True)
    source_type = models.CharField(max_length=500, blank=True, null=True)
    attachments = models.JSONField(blank=True, null=True)
    raw_id = models.CharField(max_length=500, blank=True, null=True)
    created_time = models.DateTimeField(blank=True, null=True)
    db_created_at = models.DateTimeField(blank=True, null=True)
    db_updated_at = models.DateTimeField(blank=True, null=True)
    conversation = models.ForeignKey(Conversation, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'message'


class Reaction(models.Model):
    id = models.UUIDField(primary_key=True)
    reaction_type = models.CharField(max_length=500, blank=True, null=True)
    db_created_at = models.DateTimeField(blank=True, null=True)
    db_updated_time = models.DateTimeField(blank=True, null=True)
    channel_customer = models.ForeignKey(ChannelCustomer, models.DO_NOTHING, blank=True, null=True)
    post = models.ForeignKey(Post, models.DO_NOTHING, blank=True, null=True)
    photo = models.ForeignKey(Photo, models.DO_NOTHING, blank=True, null=True)
    album = models.ForeignKey(Album, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'reaction'
