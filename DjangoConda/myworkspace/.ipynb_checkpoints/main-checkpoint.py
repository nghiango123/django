from myapp.mongodb import MongodbData
from myapp.models import Post
import uuid
import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'myworkspace.settings')

application = get_wsgi_application()

def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint.


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print_hi('PyCharm')
    mongo = MongodbData('192.168.20.107', '27078', 'facebook')
    data = mongo.data_post('fb_post')
    print(len(data))
    
    for ob in data:
        post = Post()
        post.id = uuid.uuid4()
        post.message = ob['message']
        post.source_created_time = ob['created_time']
        post.source_id = ob['post_id']
        post.source_url = ob['permalink_url']
        post.media_type = ob['media_type']
        post.title = ob['title']
        post.total_likes = ob['post_reactions_by_type_total']
        post.total_video_views_unique = ob['post_video_views_unique']
        post.type_data_crawl = 'Graph API'
        post.save()
        break